public class Tang {

    public static double bernoulli(int xInt) {
        double summary = 0;
        if (xInt == 0) return 1;
        for (int i = 1; i <= xInt; i++) {
            summary += (getFactorial((xInt + 1)) / (getFactorial(i + 1) * getFactorial(xInt - i))) * bernoulli(xInt - i);
        }
        double xDouble = xInt;
        double result = ((-1) / (xDouble + 1)) * summary;
        return result;
    }

    public static long getFactorial(int number) {
        long result = 1;
        for (int i = 1; i <= number; i++) {
            result = result * i;
        }
        return result;
    }

    public static double tang(double x) {
        if ((x == Double.NaN) || (x == Double.POSITIVE_INFINITY) || (x == Double.NEGATIVE_INFINITY)){
            return Double.NaN;
        }
        double xABS = Math.abs(x);
        while (Math.abs(xABS) > (Math.PI / 2)) {
            xABS -= Math.PI;
        }
        double summary = 0.0;
        double accuracyCheck = 1.0;
        int i = 0;
        while (Math.abs(accuracyCheck) >= 0.0001) {
            i++;
            accuracyCheck = (bernoulli(2 * i) * Math.pow(-4, i) * (1 - Math.pow(4, i)) * Math.pow(xABS, (2 * i - 1))) / (getFactorial(2 * i));
            summary += accuracyCheck;
        }
        if (x >= 0) {
            return summary;
        } else {
            return -summary;
        }
    }
}