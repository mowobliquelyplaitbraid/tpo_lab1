public class Heap<T extends Comparable<T>> {
//      Создать левостороннюю кучу.
    public Heap() {
        this.root = null;
    }


//      Соединить heapSorted с this в приоритетную очередь.
//      heapSorted становится пустой. heapSorted должна отличаться от this.
    public void merge(Heap<T> heapSorted) {
        if (this == heapSorted)
            return;

        this.root = merge(this.root, heapSorted.root);
        this.size = this.size + heapSorted.size;
        heapSorted.makeEmpty();
    }

//      Внутренний метод для объединения двух корней.
//      Обрабатывает исключительные случаи и вызывает метод merge1.
    private node<T> merge(node<T> tree1, node<T> tree2) {
        if (tree1 == null)
            return tree2;
        if (tree2 == null)
            return tree1;
        if (tree1.element.compareTo(tree2.element) < 0)
            return mergeSubtrees(tree1, tree2);
        else
            return mergeSubtrees(tree2, tree1);
    }

//      Внутренний метод для объединения двух корней.
//      Предполагает, что кучи не пусты, и subtree1's корень содержит меньший элемент.
    private node<T> mergeSubtrees(node<T> subtree1, node<T> subtree2) {
        if (subtree1.left == null)   // Одиночный узел
            subtree1.left = subtree2;       // Другие поля subtree1 уже в порядке
        else {
            subtree1.right = merge(subtree1.right, subtree2);
            if (subtree1.left.nullPathLength < subtree1.right.nullPathLength)
                swapChildren(subtree1);
            subtree1.nullPathLength = subtree1.right.nullPathLength + 1;
        }
        return subtree1;
    }

//      Меняет местами детей n.
    private static <T> void swapChildren(node<T> n) {
        node<T> tmp = n.left;
        n.left = n.right;
        n.right = tmp;
    }

//      Вставка в левостороннюю кучу с поддержанием приоритетной очереди.
    public void insert(T x) throws Exception {
        if (x == null)
            throw new Exception("The element is null.");
        root = merge(new node<T>(x), root);
        size += 1;
    }

//      Находит наименьший элемент приоритетной кучи.
      public T findMin() throws Exception {
        if (isEmpty())
            throw new Exception("The heap is empty.");
        return root.element;
    }


//     Удаляет наименьший элемент приоритетной кучи.
    public T deleteMin() throws Exception{
        if (isEmpty())
            throw new Exception("The heap is empty.");

        T minItem = root.element;
        root = merge(root.left, root.right);

        size -= 1;

        return minItem;
    }

//     Проверяет, пуста ли левосторонняя куча.
    public boolean isEmpty() {
        return root == null;
    }

//      Очищает левостороннюю кучу.
    public void makeEmpty() {
        root = null;
        size = 0;
    }

//      Ищет элемент в куче, возвращает true, если элемент найден.
//    public boolean exists(T element) {
//        node<T> nextNode = this.root;
//        while (!nextNode.right.equals(null)) {
//            if (nextNode.right == element)
//                return true;
//            nextNode = nextNode.right;
//        }
//
//        while (!nextNode.left.equals(null)) {
//            if (nextNode.left == element)
//                return true;
//            nextNode = nextNode.left;
//        }
//
//        return false;
//    }

    private static class node<T> {

        node(T theElement) {
            this(theElement, null, null);
        }

        node(T theElement, node<T> lt, node<T> rt) {
            element = theElement;
            left = lt;
            right = rt;
            nullPathLength = 0;
        }

        T element;      // Информация в узле
        node<T> left;         // Левый ребенок
        node<T> right;        // Правый ребенок
        int nullPathLength;          // null path length
    }

    private node<T> root;    // root

    public int getSize() {
        return size;
    }

    private int size = 0;
}