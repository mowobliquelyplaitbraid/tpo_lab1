package third;

public class Thought {
    private String result;
    private String reason;

    public Thought(String s){
        if (!s.equals("Как, опять?"))
            this.result = "42.";
        else this.result = "Упал.";
        this.reason = "Unknown";
    }

    public void setReason(String r) {
        this.reason = r;
    }

    public String getReason() {
        return reason;
    }

    public String getResult() {
        return result;
    }
}
