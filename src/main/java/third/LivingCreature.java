package third;

public class LivingCreature {
    private Place homeland;

    public LivingCreature(Place homeland){
        this.homeland = homeland;
    }

    public String think(String s){
        Thought someThought = new Thought(s);
        return someThought.getResult();
    }

    public Place getHomeland() {
        return homeland;
    }
}
