package third;

public class Flower extends LivingCreature {
    private Pot pot;
    private String name;

    public Flower(String name, Place home){
        super(home);
        this.pot = new Pot();
        this.name = name;
    }

    public String checkThought(){
        if (this.pot.getState().equals("falling")){
            return think("Как, опять?");
        } else return think("му");
    }

    public void setPotsState(String state){
        pot.setState(state);
    }
}
