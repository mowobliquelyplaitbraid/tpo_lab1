package third;

public class Pot {
    public boolean beAGoodPot;
    private String state;

    public Pot(){
        this.beAGoodPot = true;
        this.state = "Being a good pot";
    }

    public void setState(String state){
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
