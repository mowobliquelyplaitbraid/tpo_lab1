package third;

public class People extends LivingCreature {
    private String name;

    public People(String name, Place home){
        super(home);
        this.name = name;
    }

    public void addFactAboutGalaxy(Thought thought, Galaxy galaxy, String fact) {
        if (!thought.getReason().equals("Unknown"))
            galaxy.newFactAboutGalaxy(fact);
    }
}
