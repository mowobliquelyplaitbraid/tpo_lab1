package third;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LivingCreatureTest {
    LivingCreature lc;
    Place MP;

    @BeforeEach
    void setUp() {
        MP = new Galaxy();
        lc = new LivingCreature(MP);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testThinking() {
        Assertions.assertEquals("42.", lc.think("Думаю-думаю."));
    }
}