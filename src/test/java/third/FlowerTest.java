package third;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FlowerTest {
    public Flower petunia;
    Galaxy MP;

    @BeforeEach
    void setUp() {
        MP = new Galaxy();
        petunia = new Flower("Petunia", MP);
    }

    @Test
    void testThought() {
        Assertions.assertNotEquals("Как, опять?", petunia.checkThought());
        petunia.setPotsState("falling");
        Assertions.assertEquals("Упал.", petunia.checkThought());
    }

    @Test
    void testHomeland() {
        Assertions.assertEquals(petunia.getHomeland(), MP);
    }
}