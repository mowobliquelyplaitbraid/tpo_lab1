package third;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class PeopleTest {
    public People iana;
    Galaxy MP;

    @BeforeEach
    void setUp() {
        MP = new Galaxy();
        iana = new People("Iana", MP);
    }

    @Test
    void testLogic() {
        Assertions.assertEquals(iana.getHomeland(), MP);
    }

    @Test
    void testThinking() {
        Assertions.assertEquals(new ArrayList(), MP.dataAboutNature);
        iana.addFactAboutGalaxy(new Thought("Думаю о Вселенной."), MP, "Новая мысль о Вселенной.");
        Assertions.assertEquals(new ArrayList(), MP.dataAboutNature);
        Thought th = new Thought("Знаю все о мыслях горшка.");
        th.setReason("И о причинах все знаю.");
        iana.addFactAboutGalaxy(th, MP, "Самый важный факт");
        Assertions.assertTrue(MP.dataAboutNature.contains("Самый важный факт"));
    }
}