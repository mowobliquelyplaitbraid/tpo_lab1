package third;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PotTest {
    Pot pot;

    @BeforeEach
    void setUp() {
        pot = new Pot();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testPotState() {
        Assertions.assertEquals("Being a good pot", pot.getState());
        pot.setState("ANGRY.");
        Assertions.assertEquals("ANGRY.", pot.getState());
    }
}