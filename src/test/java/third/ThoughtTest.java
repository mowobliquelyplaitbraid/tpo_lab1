package third;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ThoughtTest {
    Thought th1;
    Thought th2;

    @BeforeEach
    void setUp() {
        th1 = new Thought("Как, опять?");
        th2 = new Thought("Какая-то мысль");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testThoughtResult() {
        Assertions.assertEquals("Упал.", th1.getResult());
        Assertions.assertEquals("Unknown", th1.getReason());
        Assertions.assertEquals("42.", th2.getResult());
    }
}