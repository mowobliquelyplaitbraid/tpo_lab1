import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TangTest {
    Tang taylor;

    @BeforeEach
    void setUp() {
        taylor = new Tang();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "data.csv")
    public void test(Double expectedResult, Integer numerator, Integer denominator) {
        assertEquals(expectedResult, Math.round(taylor.tang(numerator * Math.PI / denominator) * 100000.0) / 100000.0, 0.005);
    }

    @Test
    public void boundary() {
        assertEquals(0.0, taylor.tang(0), 0.005);
        assertEquals(0.0, taylor.tang(Math.PI), 0.005);
        assertEquals(1.0, taylor.tang(Math.PI / 4), 0.005);
        assertEquals(-1.0, taylor.tang(3 * Math.PI / 4), 0.005);
        assertEquals(Double.NaN, taylor.tang(Double.POSITIVE_INFINITY), 0.005);
        assertEquals(Double.NaN, taylor.tang(Double.NEGATIVE_INFINITY), 0.005);
        assertEquals(Double.NaN, taylor.tang(Double.NaN), 0.005);
    }

    @Test
    public void firstQuarter() {
        assertEquals(Math.sqrt(3) / 3, taylor.tang(Math.PI / 6), 0.005);
        assertEquals(1, taylor.tang(Math.PI / 4), 0.005);
        assertEquals(1*Math.sqrt(3), taylor.tang(Math.PI / 3), 0.005);
    }

    @Test
    public void secondQuarter() {
        assertEquals(-1 * Math.sqrt(3), taylor.tang(2 * Math.PI / 3), 0.005);
        assertEquals(-1, taylor.tang(3 * Math.PI / 4), 0.005);
        assertEquals(-1 * Math.sqrt(3) / 3, taylor.tang(5 * Math.PI / 6), 0.005);
    }

    @Test
    public void thirdQuarter() {
        assertEquals(Math.sqrt(3) / 3, taylor.tang(7 * Math.PI / 6), 0.005);
        assertEquals(1, taylor.tang(5 * Math.PI / 4), 0.005);
        assertEquals(Math.sqrt(3), taylor.tang(4 * Math.PI / 3), 0.005);
    }

    @Test
    public void forthQuarter() {
        assertEquals(-Math.sqrt(3) / 3, taylor.tang(-Math.PI / 6), 0.005);
        assertEquals(-1, taylor.tang(-Math.PI / 4), 0.005);
        assertEquals(-Math.sqrt(3), taylor.tang(-Math.PI / 3), 0.005);
    }
}