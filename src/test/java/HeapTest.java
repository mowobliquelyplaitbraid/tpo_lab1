import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class HeapTest {
    Heap<Integer> heap1;
    Heap<Integer> heap2;

    @BeforeEach
    void setUpHeaps() {
        heap1 = new Heap<>();
        heap2 = new Heap<>();
    }

    Integer setUpHeapsWithNumbers(int n) {
        Random random = new Random();
        List<Integer> elements = new ArrayList<>();
        int element, i;
        int minForHeap1 = 100001;

        for (i = 0; i < n; i++) {
            element = random.nextInt(100000);
            if (!elements.contains(element))
                elements.add(element);
            else {
                i--;
                continue;
            }
            try {
                heap1.insert(element);
                if (element < minForHeap1) {
                    minForHeap1 = element;
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }

        for (i = 1; i <= n; i++) {
            try {
                heap2.insert(random.nextInt());
            } catch (Exception e) {
                e.getMessage();
            }
        }

        return minForHeap1;
    }

    @RepeatedTest(value = 5, name = "#{currentRepetition} of {totalRepetitions}")
    void merge() {
        setUpHeapsWithNumbers(25);
        heap1.merge(heap2);
        assertEquals(50, heap1.getSize());
    }

    @Test
    void makeEmpty() {
        setUpHeapsWithNumbers(25);
        assertEquals(25, heap1.getSize());
        heap1.makeEmpty();
        assertEquals(0, heap1.getSize());
    }

    @Test
    void insert() {
        try {
            heap1.insert(1);
            assertEquals(1, heap1.getSize());
            heap1.insert(2);
            heap1.insert(3);
            assertEquals(3, heap1.getSize());
        } catch (Exception e) {
            e.getMessage();
        }
        Assertions.assertThrows(Exception.class, () -> heap1.insert(null));
    }

    @Test
    void findMin() {
        assertThrows(Exception.class, () -> heap1.findMin());
        Integer minElement = setUpHeapsWithNumbers(8);
        try {
            assertEquals(minElement, (int) heap1.findMin());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void deleteMin() {
        assertThrows(Exception.class, () -> heap1.deleteMin());
        try {
            Integer min = setUpHeapsWithNumbers(15);
            assertEquals(min, (int) heap1.deleteMin());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void isEmpty() {
        assertTrue(heap1.isEmpty());
        setUpHeapsWithNumbers(2);
        assertFalse(heap1.isEmpty());
    }
}